os.bin: kernel.bin boot.bin
	cat boot.bin kernel.bin > os.bin
kernel.bin:
	cd kernel; make;
boot.bin:
	cd boot; make;

clean:
	rm *.bin