
;;; writing by amine kabab on 2013 august 
;;; email : kabab1993@gmail.com
;;; This boot is to display a message when loading kernel
;;; the boot size is 512 bytes and it ends with the magic number 0xaa55
;;; and it is loaded by the bios on the memory at the address
;;; 0x7c00 after that the bios routine give the main to boot program
;;; i used the stack to pass arguments between routines, i choose the
;;; the address 0x8000 as the beginnig of it cause there a free
;;; space down this address

[org 0x7c00]
	
%define BOOTSEG		0x7c0	; boot segment at 0x7c00
%define STACKSEG 	0x9000	; stack will strat from this address
	
_start:
	mov 	[BOOT_DRIVE], dl
	mov 	sp, STACKSEG
	mov	bp, sp
	call 	_disk_load
	
	call	switch_to_pm
	jmp $	

_disk_load:
	pusha
	
	mov 	ah, 0x2
	mov	al, 0x5		 ;sector to read
	mov	ch, 0		 ;cylinder number
	mov	cl, 2		 ;sector number
	mov	dh, 0		 ;head number
	mov	dl, [BOOT_DRIVE] ;drive number
	mov	bx, 0
	mov	es, bx
	mov	bx, 0x9000
	int	0x13
	jc	disk_error
	
	cmp	al, 0x5
	je	end_disk
	
	disk_error:
	mov	bx, error
	call 	_print
	end_disk:	
	popa
	ret

_print:
	pusha
	mov	ah, 0x0e
	print_loop:
	mov	al, [bx]
	int 	0x10
	inc 	bx
	cmp	byte [bx], 0
	jne	print_loop
	popa
	ret
	
%include "gdt.s"
%include "switch_pm.s"
	
[bits 32]

_begin_pm:
	loop:
	
	;; xor 	ax,ax		
	;; in	al, 0x60
	;; mov	[0xb8000],al
	;; jmp	loop
	call 0x9000		
	jmp $
	error:	db "disk error", 0
	BOOT_DRIVE:	db 0
	times 	510-($-$$) db 0
	dw	0xaa55
