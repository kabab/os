#define COLOR 0x0f
#define VIDADD 0xb8000

char *vid_mem;
int posx = 0;
int posy = 0;
int del_lim = 0;
typedef struct{
  unsigned char code;
  char key;
}key;

key keydown_fr[] = {
  {0x1e, 'A'},
  {0x2c, 'Z'},
  {0x12, 'E'},
  {0x13, 'R'},
  {0x14, 'T'},
  {0x15, 'Y'},
  {0x16, 'U'},
  {0x17, 'I'},
  {0x18, 'O'},
  {0x19, 'P'},
  {0x10, 'Q'},
  {0x1f, 'S'},
  {0x20, 'D'},
  {0x21, 'F'},
  {0x22, 'G'},
  {0x23, 'H'},
  {0x24, 'J'},
  {0x25, 'K'},
  {0x26, 'L'},
  {0x32, 'M'},
  {0x11, 'W'},
  {0x2d, 'X'},
  {0x2e, 'C'},
  {0x2f, 'V'},
  {0x30, 'B'},
  {0x31, 'N'},
  {0x33, '?'},
  {0x27, '.'},
  {0xff, '\t'},
  {0x1c, '\n'},
  {0xee, '\b'},
  {0x39, ' '},
  {0xdd, '+'},
  {0,0}
};

key keyup_fr[] = {
  {0x9e, 'A'},
  {0xac, 'Z'},
  {0x92, 'E'},
  {0x93, 'R'},
  {0x94, 'T'},
  {0x95, 'Y'},
  {0x96, 'U'},
  {0x97, 'I'},
  {0x98, 'O'},
  {0x99, 'P'},
  {0x90, 'Q'},
  {0x9f, 'S'},
  {0xa0, 'D'},
  {0xa1, 'F'},
  {0xa2, 'G'},
  {0xa3, 'H'},
  {0xa4, 'J'},
  {0xa5, 'K'},
  {0xa6, 'L'},
  {0xb2, 'M'},
  {0x91, 'W'},
  {0xad, 'X'},
  {0xae, 'C'},
  {0xaf, 'V'},
  {0xb0, 'B'},
  {0xb1, 'N'},
  {0xb3, '?'},
  {0xa7, '.'},
  {0x8f, '\t'},
  {0x9c, '\n'},
  {0x8e, '\b'},
  {0xb9, ' '},
  {0x8d, '+'},
  {0,0}
};

putc(char c);
char ctoc(unsigned code, key *table);
getchar();
puts(char *str);
print_char(char c, int row, int col);
unsigned char port_byte_in ( unsigned short port );
itoh(int n, char *c);
clear_screen();
itoa(int n, char *c);

main(){
  vid_mem =(char*) VIDADD;
  clear_screen();
  char c[10];
  int in;
  puts("aminix# ");
  putc('A');
  putc('B');
  getchar();

}
puts(char *str){
  int i = 0;
  while(str[i]!=0){
    if(str[i] == '\n'){
      posy++;
      posx = 0;
    }else{
      print_char(str[i], posy, posx);
      posx++;
    }
    
    if(posx == 80){
      posx = 0;
      posy++;
    }
    i++;
  }

}

print_char(char c, int row, int col){
  vid_mem[(row*80+col)*2] = c;
  vid_mem[(row*80+col)*2+1] = COLOR;
}

clear_screen(){
  int i = 80*25*2;
  posx = 0;
  posy = 0;
  while(i){
    vid_mem[i] = 0x0;
    i--;
  }
}

putc(char c){
  if(c == '\n'){
    posy++;
    return;
  }
  if(c == '\b' && posx >= del_lim){
    print_char( 0, posy, posx);
    posx--;
    if(posx<0 ){
      posx = 0;
      if(posy>0)
	posy--;
    }
  }else{
    
    print_char( c, posy, posx);
    posx++;
  }
}

itoa(int n, char *c){
  int i = 0,temp;
  while(n!=0){
    temp = n%10;
    c[i++] = '0'+temp;
    n = n/10;
  }
  c[i] = '\0';
  str_reverse(c);
}

str_reverse(char *str){
  int i = 0;
  char p;
  int lim = strlen(str);
  for(i = 0;i<lim/2;i++){
    p = str[i];
    str[i] = str[lim-i-1];
    str[lim-i-1] = p;
  }
}

int strlen(char *str){
  int i;
  for(i = 0;str[i]!='\0';i++);
  return i;
}

itoh(int n, char *c){
  int i = 0;
  int temp;
  while(n !=0 ){
    temp = n & 0xf;
    if(temp < 10){
      c[i++] = '0'+temp;
    }else{
      c[i++] = 'a'+temp-10;
    }
    n = n / 16;
  }
  str_reverse(c);
}

unsigned char port_byte_in ( unsigned short port ) {
  unsigned char result ;
  __asm__ ( "in %%dx , %%al " : "=a" ( result ) : "d" ( port ));
  return result ;
}

getchar(){
  char buf;
  char temp = 0;
  int in ;
  puts("I m here");
  while(1){
    in = port_byte_in(0x60);
    buf = ctoc((unsigned char)in ,keydown_fr);
    print_char(buf, posy, posx);
  }
}

// Code to character
char ctoc(unsigned code, key *table){
  int i = 0;
  while(table[i].code){
    if(code == table[i].code){
      puts("i m here");
      return table[i].key;
    }
    i++;
  }

  return 0;
}
